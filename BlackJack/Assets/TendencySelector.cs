using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TendencySelector : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        AIPlayerTendencies[] possibleTendencies = Resources.LoadAll<AIPlayerTendencies>("PlayerTendencies");
        List<string> tendenciesNames = new List<string>();
        foreach(AIPlayerTendencies tendency in possibleTendencies)
        {
            tendenciesNames.Add(tendency.name);
        }
        GetComponent<TMP_Dropdown>().AddOptions(tendenciesNames);
    }

    
}
