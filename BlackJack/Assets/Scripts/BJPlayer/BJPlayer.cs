using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BJPlayer : MonoBehaviour
{
    public enum PlayerState
    {
        WaitingForCard,
        WaitingForCardFlip,
        WaitingForOtherPlayer,
        Busted,
        Stopped
    }

    public PlayerState playerState;

    public CardCollector cardCollector;

    public virtual void PickUpCard(Card card)
    {
        if (playerState == PlayerState.WaitingForCard)
        {
            cardCollector.AddCard(card);
        }
        else
        {
            card.WrongThrow();
        }
    }

    public virtual void UpdateState()
    {
        EvaluateNextMove();
    }

    public virtual void EvaluateNextMove()
    {
        if(cardCollector.GetCardScore() == 21)
        {
            Stop();
        }
    }

    protected virtual void Stop()
    {
        playerState = PlayerState.Stopped;
    }

    protected virtual void Bust()
    {
        playerState = PlayerState.Busted;
    }

    protected virtual void AskForACard()
    {
        playerState = PlayerState.WaitingForCard;
        cardCollector.Outline(true);
        Deck.Instance.EnableOutline();
    }

    protected virtual void WaitForCardFlip()
    {
        playerState = PlayerState.WaitingForCardFlip;
        cardCollector.Outline(false);
        Deck.Instance.DisableOutline();
    }

    public virtual void EndTurn()
    {
        playerState = PlayerState.WaitingForOtherPlayer;
        GameManager.Instance.NextTurn();
    }

    
}
