using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AIPlayerTendencies", menuName = "Scriptable Object/AIPlayerTendencies")]
public class AIPlayerTendencies : ScriptableObject
{
    [Tooltip("the liklyhood a player will ask for another card given his current score")]
    public AnimationCurve recklessness;
}

