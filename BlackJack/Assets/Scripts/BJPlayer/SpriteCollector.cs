using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpriteCollector", menuName = "Scriptable Object/AIPlayerSprites")]
public class SpriteCollector : ScriptableObject
{
    public enum Emotions
    {
        Happy,
        Medium,
        Sad
    }

    [SerializeField]
    Sprite sad;
    [SerializeField]
    Sprite medium;
    [SerializeField]
    Sprite happy;
    [SerializeField]
    Sprite card;
    [SerializeField]
    Sprite flip;
    [SerializeField]
    Sprite stop;
    [SerializeField]
    Sprite busted;

    public Sprite GetEmotionSprite(Emotions emotion)
    {
        switch (emotion)
        {
            case Emotions.Happy:
                return happy;
            case Emotions.Medium:
                return medium;
            default:
                return sad;
        }
    }

    public Sprite GetStateIcon(BJPlayer.PlayerState state)
    {
        switch (state)
        {
            case BJPlayer.PlayerState.WaitingForCard:
                return card;
            case BJPlayer.PlayerState.WaitingForCardFlip:
                return flip;
            case BJPlayer.PlayerState.Stopped:
                return stop;
            case BJPlayer.PlayerState.Busted:
                return busted;
            default:
                return null;
        }
    }
}

