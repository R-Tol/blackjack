using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AIPlayerQuotes", menuName = "Scriptable Object/AIPlayerQuotes")]
public class AIPlayerQuotes : ScriptableObject
{
    public enum QuoteType
    {
        Greetings,
        WaitingForCard,
        WaitingForFlip,
        Stopped,
        Busted,
        WrongThrow,
        Win,
        Tip
    }

    public QuoteCollection[] quotes;

    public string GetQuotes(QuoteType quoteType)
    {
        foreach(QuoteCollection quoteCollection in quotes)
        {
            if(quoteCollection.type == quoteType)
            {
                return GetRandomQuotes(quoteCollection.quotes);
            }
        }
        Debug.Log("Couldn't find quotetype");
        return "";
    }

    string GetRandomQuotes(string[] quotes)
    {
        return quotes[Random.Range(0, quotes.Length)];
    }

    [System.Serializable]
    public struct QuoteCollection
    {
        public QuoteType type;
        public string[] quotes;
    }
}

