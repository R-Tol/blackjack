using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class AIPlayer : BJPlayer
{
    
    [SerializeField]
    protected AIPlayerTendencies playerTendencies;

    #region Utilities
    [SerializeField]
    Outline outline;
    int cardLayer = 7;
    #endregion

    #region UI
    [SerializeField]
    SpriteCollector spriteCollector;
    [SerializeField]
    Image gameplayStateimage;
    [SerializeField]
    TextMeshProUGUI uiText;
    [SerializeField]
    Image talkBubble;
    [SerializeField]
    AIPlayerQuotes quotes;
    Coroutine speakCoroutine;
    #endregion

    #region emotions
    [SerializeField]
    Image emotionalStateimage;
    [SerializeField][Tooltip("AIplayer initial happiness")] //to do update image
    float happiness = 7f;
    public float Happiness { get => happiness; set { happiness = value; UpdateEmotionIcon(); } }
    [SerializeField]
    float happinessLoseOnWrongThrow = 1f;
    #endregion

    [SerializeField]
    Animator animator;

    protected void Start()
    {
        playerState = PlayerState.WaitingForOtherPlayer;
    }

    private void OnTriggerStay(Collider collider)
    {
        if(collider.gameObject.layer == cardLayer)
        {
            Card card = collider.GetComponent<Card>();
            if (!card.isPickedUp)
            {
                PickUpCard(card);
            }
        }
    }

    public override void PickUpCard(Card card)
    {
        if (playerState == PlayerState.WaitingForCard)
        {
            Happiness += card.throwGoodness;
            GameManager.Instance.AddGoodThrowBonus(this, card.throwGoodness);
            cardCollector.AddCard(card);
        }
        else
        {
            Happiness -= happinessLoseOnWrongThrow;
            animator.SetTrigger("WrongThrow");
            if (!Deck.Instance.isShuffled)
            {
                Talk("Could you shuffle the deck please?", 1.5f);
                card.WrongThrow();
            }
            else
            {
                Talk(AIPlayerQuotes.QuoteType.WrongThrow, 1.5f);
                card.WrongThrow();
            }
        }
    }

    public override void UpdateState()
    {
        if(playerState == PlayerState.WaitingForCard)
        {
            WaitForCardFlip();
        }
        else
        {
            if (cardCollector.cards.Count < 2) //yet to receive 2 cards
            {
                AskForACard();
                return;
            }
            if(cardCollector.cards.Count == 2) // second card received
            {
                EndTurn();
                return;
            }
            EvaluateNextMove();
        }
    }

    public override void EvaluateNextMove()
    {
        int currentScore = cardCollector.GetCardScore();
        if (currentScore > 21)
        {
            Bust();
            return;
        }
        float rand = UnityEngine.Random.value;
        if (playerTendencies.recklessness.Evaluate(currentScore) >= rand) //he wants another card
        {
            AskForACard();
        }
        else
        {
            Stop();
        }
        
    }

    //Pop up message of type quotetype for duration duration
    public void Talk(AIPlayerQuotes.QuoteType quoteType, float duration)
    {
        Talk(quotes.GetQuotes(quoteType), duration);
    }
    //Pop up message for duration duration
    public void Talk(string message, float duration)
    {
        uiText.text = message;
        if (speakCoroutine != null)
            StopCoroutine(speakCoroutine);
        StartCoroutine(FadeCoroutine(duration));
    }

    IEnumerator FadeCoroutine(float duration)
    {
        uiText.color = new Color(uiText.color.r, uiText.color.g, uiText.color.b, 1);
        talkBubble.color = new Color(talkBubble.color.r, talkBubble.color.g, talkBubble.color.b, 1);
        float timePassed = 0;
        while (timePassed < duration)
        {
            yield return new WaitForEndOfFrame();
            timePassed += Time.deltaTime;
            uiText.color = new Color(uiText.color.r, uiText.color.g, uiText.color.b, 1 - (timePassed / duration));
            talkBubble.color = new Color(talkBubble.color.r, talkBubble.color.g, talkBubble.color.b, 1 - (timePassed / duration));
        }
        uiText.color = new Color(uiText.color.r, uiText.color.g, uiText.color.b, 0f);
        talkBubble.color = new Color(talkBubble.color.r, talkBubble.color.g, talkBubble.color.b, 0f);

    }

    //Pop up message until func is verified
    public void Talk(string message, Func<bool> func)
    {
        uiText.text = message;
        StopAllCoroutines();
        StartCoroutine(FadeCoroutine(func));
    }

    IEnumerator FadeCoroutine(Func<bool> func)
    {
        uiText.color = new Color(uiText.color.r, uiText.color.g, uiText.color.b, 1);
        talkBubble.color = new Color(talkBubble.color.r, talkBubble.color.g, talkBubble.color.b, 1);
        yield return new WaitUntil(func);
        float timePassed = 0;
        float duration = 0.5f;
        while (timePassed < duration)
        {
            yield return new WaitForEndOfFrame();
            timePassed += Time.deltaTime;
            uiText.color = new Color(uiText.color.r, uiText.color.g, uiText.color.b, 1 - (timePassed / duration));
            talkBubble.color = new Color(talkBubble.color.r, talkBubble.color.g, talkBubble.color.b, 1 - (timePassed / duration));
        }
        uiText.color = new Color(uiText.color.r, uiText.color.g, uiText.color.b, 0f);
        talkBubble.color = new Color(talkBubble.color.r, talkBubble.color.g, talkBubble.color.b, 0f);
    }

    protected override void AskForACard()
    {
        base.AskForACard();
        Talk(AIPlayerQuotes.QuoteType.WaitingForCard, 1.5f);
        EnableOutline();
        animator.SetTrigger("AskCard");
        UpdateStateIcon();
    }

    protected override void Bust()
    {
        base.Bust();
        Talk(AIPlayerQuotes.QuoteType.Busted, 1.5f);
        animator.SetTrigger("Busted");
        UpdateStateIcon();
        GameManager.Instance.NextTurn();
    }

    protected override void Stop()
    {
        base.Stop();
        Talk(AIPlayerQuotes.QuoteType.Stopped, 1.5f);
        animator.SetTrigger("Stop");
        UpdateStateIcon();
        GameManager.Instance.NextTurn();
        GameManager.Instance.stoppedPlayers.Add(this);
    }

    protected override void WaitForCardFlip()
    {
        base.WaitForCardFlip();
        Talk(AIPlayerQuotes.QuoteType.WaitingForFlip, 1.5f);
        UpdateStateIcon();
        DisableOutline();
    }

    public override void EndTurn()
    {
        base.EndTurn();
        UpdateStateIcon();
    }

    public void EnableOutline()
    {
        outline.Blink(5.0f);
    }

    public void DisableOutline()
    {
        outline.enabled = false;
    }

    void UpdateStateIcon()
    {
        Sprite newSprite = spriteCollector.GetStateIcon(playerState);

        if (newSprite != null)
        {
            gameplayStateimage.sprite = newSprite;
            gameplayStateimage.color = new Color(1, 1, 1, 1);
        }
        else
        {
            gameplayStateimage.color = new Color(1, 1, 1, 0); // transparent
        }
            
    }

    private void UpdateEmotionIcon()
    {
        SpriteCollector.Emotions emotionState;
        if (happiness < 3)
        {
            emotionState = SpriteCollector.Emotions.Sad;
        }
        else
        {
            if(happiness < 6)
            {
                emotionState = SpriteCollector.Emotions.Medium;
            }
            else
            {
                emotionState = SpriteCollector.Emotions.Happy;
            }
        }

        emotionalStateimage.sprite = spriteCollector.GetEmotionSprite(emotionState);
    }

    public void Win()
    {
        Talk(AIPlayerQuotes.QuoteType.Win, 1.0f);
        animator.SetTrigger("Win");
    }

    public void Lose()
    {
        Talk(AIPlayerQuotes.QuoteType.Tip, 1.0f);
        animator.SetTrigger("Lose");
        happiness -= 2.0f;
    }

    public float CalculateDealersTip()
    {
        return Math.Max( (float) Math.Round(happiness), 0f);
    }


    public void SetTendency(AIPlayerTendencies tendency)
    {
        if(tendency != null)
        {
            playerTendencies = tendency;
        }
        else
        {
            AIPlayerTendencies[] possibleTendencies = Resources.LoadAll<AIPlayerTendencies>("PlayerTendencies");
            playerTendencies = possibleTendencies[UnityEngine.Random.Range(0, possibleTendencies.Length)];
        }
        
    }
}
