using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dealer : BJPlayer
{
    [SerializeField]
    Button stopButton;

    public float tips;

    private void Start()
    {
        playerState = PlayerState.WaitingForOtherPlayer;
        GameManager.Instance.dealer = this;
        stopButton.onClick.AddListener(() => { Stop(); stopButton.gameObject.SetActive(false); } );
    }

    public override void PickUpCard(Card card)
    {
        if (playerState == PlayerState.WaitingForCard)
        {
            if (cardCollector.cards.Count == 1) //second dealer's card has to be facedown and pass the turn
            {
                WaitForCardFlip();
                cardCollector.AddDealersFaceDownCard(card);
            }
            else
            {
                cardCollector.AddCard(card);
                WaitForCardFlip();
            }
        }
        else
        {
            card.WrongThrow();
        }
    }

    public override void EvaluateNextMove()
    {
        stopButton.gameObject.SetActive(false);

        if (cardCollector.cards.Count <= 1) //received only 1 card
        {
            AskForACard();
            return;
        }
        
        if(cardCollector.cards.Count == 2 && GameManager.Instance.gameState == GameManager.GameState.First2Cards)
        {
            EndTurn();
            return;
        }

        int score = cardCollector.GetCardScore();
        if (score > 17)
        {
            if (score <= 21)
            {
                Stop();
            }
            else
            {
                Bust();
            }
        }
        else
        {
            stopButton.gameObject.SetActive(true);
            AskForACard();
        }
    }

    protected override void Bust()
    {
        base.Bust();
        cardCollector.FlipSecretCard();
    }

    protected override void Stop()
    {
        base.Stop();
        cardCollector.Outline(false);
        Deck.Instance.DisableOutline();
        cardCollector.FlipSecretCard();
    }

    public void AddTip(float tip)
    {
        tips += tip;
        UIManager.Instance.AddMoney(tip, tips);
    }
}
