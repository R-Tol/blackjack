using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TipAnimation : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI text;
    [SerializeField]
    Image image;
    [SerializeField]
    float upSpeed;
    [SerializeField]
    float duration;

    float elapsed = 0;

    private void Update()
    {
        elapsed += Time.deltaTime;
        text.color = new Color(text.color.r, text.color.g, text.color.b, 1 - (elapsed / duration));
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1 - (elapsed / duration));
        transform.position = new Vector3(transform.position.x, transform.position.y + upSpeed * Time.deltaTime, transform.position.z);
    }

    public void SetValue(string amount)
    {
        text.text = "+" + amount;
    }
}
