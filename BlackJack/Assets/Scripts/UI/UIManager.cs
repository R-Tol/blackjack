using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIManager : Singleton<UIManager>
{
    [SerializeField]
    MessageComunicator messageComunicator;
    [SerializeField]
    TMP_Dropdown nOfPlayerSelector;
    [SerializeField]
    GameObject nOfPlayerSelectorPanel;
    [SerializeField]
    TMP_Dropdown tendencySelector;
    [SerializeField]
    UIWallet UIWallet;
    [SerializeField]
    GameObject endGamePanel;
    [SerializeField]
    Transform endGameResultsCollector;
    [SerializeField]
    GameObject resultPrefab;

    public void ComunicateToPlayer(string message, Color color, float duration)
    {
        messageComunicator.ComunicateMessage(message, color, duration);
    }

    public void StartGame()
    {
        GameManager.Instance.StartGame(nOfPlayerSelector.value + 1, tendencySelector.options[tendencySelector.value].text); //values are indices
        nOfPlayerSelectorPanel.SetActive(false);
    }

    internal void SetEndGameScreen(List<float> tips, float casinoPayment, float totalScore)
    {
        endGamePanel.SetActive(true);
        for(int i = 1; i <= tips.Count; i++)
        {
            Instantiate(resultPrefab, endGameResultsCollector).GetComponent<TextMeshProUGUI>().text = "Player" + i + " tip: " + tips[i - 1];
        }
        Instantiate(resultPrefab, endGameResultsCollector).GetComponent<TextMeshProUGUI>().text = "Casino payment: " + casinoPayment;
        TextMeshProUGUI totalScoreText = Instantiate(resultPrefab, endGameResultsCollector).GetComponent<TextMeshProUGUI>();
        totalScoreText.text = "Total Score: " + totalScore;
        totalScoreText.color = Color.green;
    }

    public void AddMoney(float tip, float tipsTotal)
    {
        UIWallet.gameObject.SetActive(true);
        UIWallet.AddMoney(tip, tipsTotal);
    }
}
