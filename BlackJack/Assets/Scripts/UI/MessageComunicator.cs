using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MessageComunicator : MonoBehaviour
{
    TextMeshProUGUI text;
    
    // Start is called before the first frame update
    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    public void ComunicateMessage(string newText, Color color, float duration)
    {
        text.text = newText;
        StopAllCoroutines();
        StartCoroutine(FadeCoroutine(color, duration));
    }

    IEnumerator FadeCoroutine(Color color, float duration)
    {
        text.color = new Color(color.r, color.g, color.b, 1f);
        float timePassed = 0;
        while(timePassed < duration) {
            yield return new WaitForEndOfFrame();
            timePassed += Time.deltaTime;
            text.color = new Color(color.r, color.g, color.b, (1 - timePassed / duration));
        }
        text.color = new Color(color.r, color.g, color.b, 0f);

    }
}
