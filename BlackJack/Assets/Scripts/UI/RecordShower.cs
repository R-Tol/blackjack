using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RecordShower : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetFloat("Record", 0).ToString();
    }

    
}
