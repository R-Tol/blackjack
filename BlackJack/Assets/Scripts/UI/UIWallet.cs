using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UIWallet : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI textAmount;
    [SerializeField]
    GameObject tipAnimationPrefab;

    public void AddMoney(float addedAmount, float totalAmount)
    {
        textAmount.text = totalAmount.ToString();
        Instantiate(tipAnimationPrefab, transform).GetComponent<TipAnimation>().SetValue(addedAmount.ToString());
    }
}
