using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAIPlayer : AIPlayer
{
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        AIPlayerTendencies[] possibleTendencies = Resources.LoadAll<AIPlayerTendencies>("PlayerTendencies");
        playerTendencies = possibleTendencies[Random.Range(0, possibleTendencies.Length)]; //pick a random tendency
    }
}
