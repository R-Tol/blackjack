using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class CardCollector : MonoBehaviour
{
    [SerializeField][Tooltip("Point from which the cards will be placed")]
    Transform startPoint;
    [SerializeField]
    float offset = 0.05f;

    public List<Card> cards;

    public BJPlayer owner;

    int cardLayer = 7;

    [SerializeField]
    GameObject cardUIPrefab;
    [SerializeField]
    Transform cardSpaceUI;
    [SerializeField]
    TextMeshProUGUI cardScoreUI;

    [SerializeField]
    Outline outline;
    public void AddCard(Card card)
    {
        if(card.value == 1) //handle aces
        {
            if(GetCardScore() <= 10)
            {
                card.value = 11;
            }

        }
        cards.Add(card);
        while (GetCardScore() > 21 && cards.Any((x) => x.value == 11))
        {
            foreach(Card oldCard in cards)
            {
                if(oldCard.value == 11)
                {
                    oldCard.value = 1;
                    break;
                }
            }
        }
        card.EnableOutline();
        card.StopAllForces();
        card.inPlay = true;
        card.transform.position = startPoint.position;
        card.transform.parent = startPoint;
        card.transform.localPosition = new Vector3(-offset * cards.Count, 0 , 0 );
        Vector3 rotation = card.transform.rotation.eulerAngles;
        card.transform.localRotation = Quaternion.Euler(rotation.x, 0, rotation.z);
        card.Flipped += UpdateCardFlip;
        owner.UpdateState();
        GameManager.Instance.ResetHappinessDepletionTimer();
    }

    public void AddDealersFaceDownCard(Card card)
    {
        if (card.value == 1) //handle aces
        {
            if (GetCardScore() <= 10)
            {
                card.value = 11;
            }
        }
        cards.Add(card);
        //place card in card zone
        card.StopAllForces();
        card.inPlay = true;
        card.transform.position = startPoint.position;
        card.transform.parent = startPoint;
        card.transform.localPosition = new Vector3(-offset * cards.Count, 0, 0);
        Vector3 rotation = card.transform.rotation.eulerAngles;
        card.transform.localRotation = Quaternion.Euler(rotation.x, 0, rotation.z);
        card.isFlipped = true;
        
        UpdateCardFlip(card);
        GameManager.Instance.ResetHappinessDepletionTimer();
    }

    public void UpdateCardFlip(Card card)
    {
        Image image = Instantiate(cardUIPrefab, cardSpaceUI).GetComponent<Image>();
        image.sprite = card.cardSprite;
        cardScoreUI.text = "Current score:" + GetCardScore();
        owner.UpdateState();
    }

    public int GetCardScore()
    {
        int totalScore = 0;
        foreach (Card card in cards)
        {
            if(card.isFlipped)
                totalScore += card.value;
        }

        return totalScore;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer == cardLayer)
        {
            Card card = collider.GetComponent<Card>();
            if (!card.isPickedUp && !cards.Contains(card))
            {
                card.throwGoodness += 0.5f; //bonus for hitting the card collector
                owner.PickUpCard(card);
            }
        }
    }

    public void Outline(bool on)
    {
        if (on)
        {
            outline.Blink();
        }
        else
        {
            outline.enabled = false;
        }
    }

    public void FlipSecretCard()
    {
        cards[1].isFlipped = false;
        cards[1].EnableOutline();
        cards[1].Flipped += EndGame;
    }

    public void EndGame(Card card)
    {
        card.DisableOutline(); //disable secret card outline
        GameManager.Instance.EndGameResults();
    }
}
