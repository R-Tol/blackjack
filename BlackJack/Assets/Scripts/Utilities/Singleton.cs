using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
	static T instance;
	public bool isDestroyable;
	public static T Instance => instance;

	protected void Awake()
	{
		if (instance == null)
		{
			instance = (T)this;
			if(!isDestroyable)
            {
				DontDestroyOnLoad(gameObject);
            }
		}
		else
		{
			Destroy(gameObject);
		}
	}
}


