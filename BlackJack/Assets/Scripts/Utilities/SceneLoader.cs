using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [Scene]
    public string gameScene;
    [Scene]
    public string mainMenu;

    public void Quit()
    {
        Application.Quit();
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(gameScene);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }
}
