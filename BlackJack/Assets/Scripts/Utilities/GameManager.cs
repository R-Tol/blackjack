using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    List<AIPlayer> enemies = new List<AIPlayer>();
    public Dealer dealer;
    public List<AIPlayer> stoppedPlayers = new List<AIPlayer>(); //stopped players
    [SerializeField][Tooltip("SpawnPoints Ordered left to right")]
    Transform[] spawnPoints;

    [SerializeField]
    GameObject AIPlayerPrefab;

    [SerializeField][Tooltip("Seconds before enemies lose happiness after being idle or failing task")]
    float happinessDepletionInterval = 10f;
    [SerializeField][Tooltip("Amount of happiness lost")]
    float happinessDepletionAmount = 0.5f;
    Coroutine happinessDepletion;

    public enum GameState
    {
        First2Cards,
        Gamble,
        EndGame
    }

    public GameState gameState = GameState.First2Cards;
    int servedPlayers = -1; //number of players that received their cards during this round

    public void StartGame(int numberOfPlayers, string tendency)
    {
        dealer.GetComponent<PlayerCameraMovement>().enabled = true;
        UIManager.Instance.ComunicateToPlayer("Good luck player!", Color.white, 2.0f);
        AIPlayerTendencies choosenTendency = null;
        if (tendency != "RANDOM")
        { 
             choosenTendency = Resources.Load<AIPlayerTendencies>("PlayerTendencies/" + tendency);
        }
        if (numberOfPlayers == 1) {
            SpawnPlayer(2, choosenTendency);
        }
        else
        {
            if(numberOfPlayers <= 3)
            {
                for(int i = 1; i < numberOfPlayers + 1; i++)
                {
                    SpawnPlayer(i, choosenTendency);
                }
            }
            else
            {
                for (int i = 0; i < numberOfPlayers; i++)
                {
                    SpawnPlayer(i, choosenTendency);
                }
            }
        }
        happinessDepletion = StartCoroutine(DepleteHappiness());
        StartCoroutine(AskPlayerToShuffleDeck());
    }

    //spawn player at spawnPoint index position
    public void SpawnPlayer(int position, AIPlayerTendencies tendency)
    {
        AIPlayer AIPlayer = Instantiate(AIPlayerPrefab, spawnPoints[position]).GetComponent<AIPlayer>();
        AIPlayer.SetTendency(tendency);
        enemies.Add(AIPlayer);
    }


    IEnumerator AskPlayerToShuffleDeck()
    {
        yield return new WaitForSeconds(2.0f);
        UIManager.Instance.ComunicateToPlayer("First click on the deck to shuffle it", Color.white, 3.0f);
        Deck deck = Deck.Instance;
        yield return new WaitUntil(() => deck.isShuffled);
        UIManager.Instance.ComunicateToPlayer("Distribute the cards", Color.white, 3.0f);
        NextTurn();
    }

    public void EndGameResults()
    {
        gameState = GameState.EndGame;
        StartCoroutine(PlayEndGameScenario());
    }

    public void NextTurn()
    {
        servedPlayers++;
        //serve next player
        if(gameState == GameState.First2Cards)
        {
            if(servedPlayers < enemies.Count)
            {
                enemies[servedPlayers].EvaluateNextMove();
            }
            else
            {
                if(servedPlayers == enemies.Count)//we served all the enemies it's the delears turn
                {
                    dealer.EvaluateNextMove();
                }
                else
                {
                    gameState = GameState.Gamble;
                    servedPlayers = -1;
                    NextTurn();
                }
            }
        }
        else
        {
            if (servedPlayers < enemies.Count)
            {
                enemies[servedPlayers].EvaluateNextMove();
            }
            else
            {
                //we served all the enemies it's the delears turn
                dealer.EvaluateNextMove();
            }
        }
    }

    IEnumerator DepleteHappiness()
    {
        while(gameState != GameState.EndGame)
        {
            yield return new WaitForSeconds(happinessDepletionInterval);
            foreach(AIPlayer enemy in enemies)
            {
                enemy.Happiness -= happinessDepletionAmount;
            }
        }
    }

    public void ResetHappinessDepletionTimer()
    {
        StopCoroutine(happinessDepletion);
        happinessDepletion = StartCoroutine(DepleteHappiness());
    }

    public void AddGoodThrowBonus(AIPlayer receiver, float throwValue)
    {
        foreach(AIPlayer enemy in enemies)
        {
            if(enemy != receiver)
            {
                enemy.Happiness += throwValue / enemies.Count;
            }
        }
    }

    IEnumerator PlayEndGameScenario()
    {
        int numberOfWins = 0;
        List<float> tips = new List<float>();
        if (dealer.playerState == BJPlayer.PlayerState.Busted)
        {
            foreach (AIPlayer concurrent in enemies)
            {
                tips.Add(concurrent.CalculateDealersTip());
                dealer.AddTip(tips[tips.Count - 1]);
                if(concurrent.playerState == BJPlayer.PlayerState.Stopped)
                {
                    concurrent.Win();
                    yield return new WaitForSeconds(1.0f);
                }
                else
                {
                    concurrent.Lose();
                    ++numberOfWins;
                    yield return new WaitForSeconds(1.0f);
                }
            }
        }
        else
        {
            int dealersScore = dealer.cardCollector.GetCardScore();
            foreach (AIPlayer concurrent in enemies)
            {
                tips.Add(concurrent.CalculateDealersTip());
                dealer.AddTip(tips[tips.Count - 1]);
                if(concurrent.playerState == BJPlayer.PlayerState.Busted)
                {
                    concurrent.Lose();
                    ++numberOfWins;
                    yield return new WaitForSeconds(1.0f);
                }
                else
                {
                    if (concurrent.cardCollector.GetCardScore() > dealersScore)
                    {
                        concurrent.Win();
                        yield return new WaitForSeconds(1.0f);
                    }
                    else
                    {
                        concurrent.Lose();
                        ++numberOfWins;
                        yield return new WaitForSeconds(1.0f);
                    }
                }
                
            }
        }
        float casinoPayment = numberOfWins * 10f;
        dealer.AddTip(casinoPayment);
        UIManager.Instance.ComunicateToPlayer("You received " + casinoPayment + " from the casino!", Color.green, 1.0f);
        yield return new WaitForSeconds(1.0f);
        UIManager.Instance.SetEndGameScreen(tips, casinoPayment, dealer.tips);
        float record = PlayerPrefs.GetFloat("Record", 0);
        if (dealer.tips > record)
        {
            PlayerPrefs.SetFloat("Record", dealer.tips);
        }
    }
}
