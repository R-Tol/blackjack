using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraMovement : MonoBehaviour
{
    float yaw;

    float pitch;

    public float speed;

    Camera camera;

    [SerializeField]
    int boundary;

    private void Start()
    {
        camera = Camera.main;
        pitch = camera.transform.localRotation.eulerAngles.x;
    }

    public void Update()
    {
        yaw += Input.GetAxisRaw("Horizontal") * Time.deltaTime * speed;

        pitch += Input.GetAxisRaw("Vertical")* -1 * Time.deltaTime * speed;

        pitch = Mathf.Repeat(pitch, 360.0f);

        camera.transform.rotation = Quaternion.Euler(pitch, camera.transform.rotation.eulerAngles.y, camera.transform.rotation.eulerAngles.z);

        if (Input.mousePosition.x > Screen.width - boundary)
        {
            yaw += Time.deltaTime * speed;
        }

        if (Input.mousePosition.x < 0 + boundary)
        {
            yaw -= Time.deltaTime * speed;
        }

        if (Input.mousePosition.y > Screen.height - boundary)
        {
            pitch -= Time.deltaTime * speed;
        }

        if (Input.mousePosition.y < 0 + boundary)
        {
            pitch += Time.deltaTime * speed;
        }

        pitch = Mathf.Repeat(pitch, 360.0f);

        camera.transform.rotation = Quaternion.Euler(pitch, camera.transform.rotation.eulerAngles.y, camera.transform.rotation.eulerAngles.z);

        yaw = Mathf.Repeat(yaw, 360.0f);

        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, yaw, transform.rotation.eulerAngles.z);
    }

}
