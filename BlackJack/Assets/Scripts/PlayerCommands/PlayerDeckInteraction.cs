using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeckInteraction : MonoBehaviour
{
    public LayerMask deckLayer;
    public LayerMask cardLayer;

    Card pickedUpCard;

    [SerializeField]
    Plane cardPlane;

    Camera mainCamera;

    Vector3 oldPos; //mouse postion 1 frame ago
    Vector3 oldOldPos; //mouse position 2 frame ago
    [SerializeField]
    float releaseSpeed = 10;

    float curveAmount = 0;
    [SerializeField]
    float curveSpeed = 2f;

    float maxCurveAmount = 2.5f;
    Rect circlingBox;

    private void Start()
    {
        mainCamera = Camera.main;
        ResetThrowValues();
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!Deck.Instance.isShuffled) //first click shuffles the deck
            {
                RaycastHit rayInfo;

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out rayInfo, Mathf.Infinity, deckLayer))
                {
                    Transform transformHit = rayInfo.transform;

                    Deck deck = rayInfo.collider.gameObject.GetComponent<Deck>();
                    if (deck != null)
                    {
                        deck.Shuffle();
                    }
                }
            }
            else
            {
                if (pickedUpCard == null) //no card's in hand
                {
                    RaycastHit rayInfo;

                    Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray, out rayInfo, Mathf.Infinity, deckLayer)) //pick card from deck
                    {
                        Transform transformHit = rayInfo.transform;

                        Deck deck = rayInfo.collider.gameObject.GetComponent<Deck>();

                        pickedUpCard = deck.ExtractCard();

                        pickedUpCard.GetComponent<Card>().PickUp();

                        cardPlane = new Plane(Vector3.up, Vector3.up * deck.topDeck.position.y);

                        oldOldPos = Input.mousePosition;
                        oldPos = Input.mousePosition;
                    }
                    else
                    {
                        if (Physics.Raycast(ray, out rayInfo, Mathf.Infinity, cardLayer)) //pick card from table
                        {
                            Transform transformHit = rayInfo.transform;
                            Card card = rayInfo.collider.gameObject.GetComponent<Card>();

                            if (!card.inPlay)
                            {
                                pickedUpCard = card;
                                pickedUpCard.PickUp();
                                oldPos = Input.mousePosition;
                            }
                            else
                                card.Flip();
                            oldOldPos = Input.mousePosition;
                            oldPos = Input.mousePosition;
                        }
                    }
                }
            }
            
        }

        if (Input.GetMouseButton(0)) //drag
        {
            if(pickedUpCard != null)
            {
                
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                float distance;
                if (cardPlane.Raycast(ray, out distance))
                {
                    pickedUpCard.transform.position = ray.GetPoint(distance);
                }
                Vector3 pos = Input.mousePosition;
                
                UpdateCircleBox();
                CalculateCurve();

                pickedUpCard.rb.maxAngularVelocity = curveAmount * 8f;
                pickedUpCard.rb.angularVelocity = pickedUpCard.transform.up * curveAmount * 8f + pickedUpCard.rb.angularVelocity;

                oldOldPos = oldPos;
                oldPos = Input.mousePosition;
            }
        }
        else
        {
            if(pickedUpCard != null) //throw the card
            {
                Vector3 pos = Input.mousePosition;
                Vector3 cameraForward = Camera.main.transform.forward;
                float rotationAngle = (float) Math.Acos(Vector3.Dot(cameraForward , Vector3.forward));
                float cosA = (float) Math.Cos(rotationAngle);
                float sinA = (float) Math.Sin(rotationAngle);
                float throwX = pos.x - oldPos.x;
                float throwZ = pos.y - oldPos.y;
                Vector3 throwVector = new Vector3(throwX * cosA - throwZ * sinA, 0, throwX * sinA + throwZ * cosA );
                Vector3 force = throwVector * releaseSpeed * Time.deltaTime; //there would be a moltiplication and a division for Time.deltaTime
                
                UpdateCircleBox();
                CalculateCurve();

                Vector3 curveDirection = Vector3.up * curveAmount * Time.deltaTime;
               
                pickedUpCard.Release(force, curveDirection, Mathf.Abs(curveAmount) / maxCurveAmount);
                
                pickedUpCard = null;
                ResetThrowValues();
            }
        }

    }

    void UpdateCircleBox()
    {
        Vector2 pos = Input.mousePosition;
        circlingBox.xMin = Mathf.Min(oldOldPos.x, oldPos.x, pos.x);
        circlingBox.xMax = Mathf.Max(oldOldPos.x, oldPos.x, pos.x);
        circlingBox.yMin = Mathf.Min(oldOldPos.y, oldPos.y, pos.y);
        circlingBox.yMax = Mathf.Max(oldOldPos.y, oldPos.y, pos.y);
    }

    void CalculateCurve()
    {
        Vector2 pos = Input.mousePosition;
        Vector2 rotationCenter = circlingBox.center;
        float sign = Math.Sign((oldPos.y - rotationCenter.y) * (pos.x - rotationCenter.x) - (pos.y - rotationCenter.y) * (oldPos.x - rotationCenter.x));
        curveAmount += sign * Time.deltaTime * curveSpeed;
        curveAmount = Mathf.Clamp(curveAmount, -maxCurveAmount, maxCurveAmount);
    }

    private void ResetThrowValues()
    {
        curveAmount = 0f;
    }

}
