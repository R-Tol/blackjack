using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleAnimation : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }
}
