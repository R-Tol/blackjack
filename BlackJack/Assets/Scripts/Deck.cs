using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : Singleton<Deck>
{
    public enum DeckState
    {
        Empty,
        HalfEmpty,
        Full
    }

    [SerializeField]
    GameObject[] cards;
    int currentLastCardIndex;
    int totalCardsAmount;

    public bool isShuffled;

    GameObject currentModel;
    [SerializeField]
    GameObject halfDeckModel;
    [SerializeField]
    GameObject fullDeckModel;
    [SerializeField]
    GameObject shuffleDeckModel;
    DeckState currentDeckAppearence;

    public int CurrentLastCardIndex { get => currentLastCardIndex; set { currentLastCardIndex = value; UpdateModel();  } }

    public Transform topDeck;

    Outline outline;

    private void Start()
    {
        int value = 0;
        foreach(GameObject card in cards) {
            card.GetComponent<Card>().sortingValue = value;
            value++;
        }
        totalCardsAmount = cards.Length;
        CurrentLastCardIndex = cards.Length - 1;
        currentDeckAppearence = DeckState.Full;
        EnableOutline();
    }

    public void Shuffle()
    {
        StartCoroutine(ShuffleAnimation());
        int length = currentLastCardIndex + 1;//shuffle only the cards in the deck
        System.Random rnd = new System.Random();
        while (length > 1)
        {
            int k = rnd.Next(length--); //find random value
            GameObject temp = cards[length]; //basic swap
            cards[length] = cards[k];
            cards[k] = temp;
        }

        isShuffled = true;
        DisableOutline();
        outline.OutlineColor = Color.green;
        GameManager.Instance.ResetHappinessDepletionTimer();
    }

    public void Sort()
    {
        for(int i = 0; i <= CurrentLastCardIndex; i++)
        {
            int min = cards[i].GetComponent<Card>().sortingValue;
            int minIndex = i;
            for(int j = i + 1; j <= CurrentLastCardIndex; j++)
            {
                if(min > cards[j].GetComponent<Card>().sortingValue)
                {
                    min = cards[j].GetComponent<Card>().sortingValue;
                    minIndex = j;
                }
            }
            GameObject temp = cards[i];
            cards[i] = cards[minIndex];
            cards[minIndex] = temp;
        }
    }

    IEnumerator ShuffleAnimation()
    {
        GameObject shuffleDeck = Instantiate(shuffleDeckModel, transform);
        currentModel.SetActive(false);
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;
        yield return new WaitForSeconds(1.0f);
        currentModel.SetActive(true);
        collider.enabled = true;
        Destroy(shuffleDeck);
    }

    public Card ExtractCard()
    {
        Card card = Instantiate(cards[CurrentLastCardIndex], topDeck).GetComponent<Card>();
        card.transform.rotation = Quaternion.Euler(new Vector3(180, 0, 0));
        card.prefab = cards[CurrentLastCardIndex];
        CurrentLastCardIndex--;
        return card;
    }

    void UpdateModel()
    {
        if (CurrentLastCardIndex > totalCardsAmount / 2 && currentDeckAppearence != DeckState.Full)
        {
            Destroy(currentModel);
            currentModel = Instantiate(fullDeckModel, transform);
            currentDeckAppearence = DeckState.Full;
            outline = currentModel.GetComponent<Outline>();
        }
        if (CurrentLastCardIndex < totalCardsAmount / 2 && CurrentLastCardIndex > 0 && currentDeckAppearence != DeckState.HalfEmpty)
        {
            Destroy(currentModel);
            currentModel = Instantiate(halfDeckModel, transform);
            currentDeckAppearence = DeckState.HalfEmpty;
            outline = currentModel.GetComponent<Outline>();
        }

        if (CurrentLastCardIndex == -1 && currentDeckAppearence != DeckState.Empty)
        {
            Destroy(currentModel);
            currentDeckAppearence = DeckState.Empty;
            //Handle empty deck
        }

        if(CurrentLastCardIndex == 0 && currentDeckAppearence == DeckState.Empty)
        {
            currentModel = Instantiate(halfDeckModel, transform);
            currentDeckAppearence = DeckState.HalfEmpty;
            outline = currentModel.GetComponent<Outline>();
        }
    }

    public void AddCard(Card card)
    {
        cards[currentLastCardIndex + 1] = card.prefab;
        CurrentLastCardIndex++;
        Destroy(card.gameObject);
    }

    public void EnableOutline()
    {
        outline.Blink();
    }

    public void DisableOutline()
    {
        outline.enabled = false;
    }
}
