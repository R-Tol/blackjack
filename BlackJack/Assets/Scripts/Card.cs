using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Card : MonoBehaviour
{
    public int value;
    public int sortingValue;

    public bool isPickedUp;
    public bool inPlay;
    public GameObject prefab;

    public Rigidbody rb;

    int deckLayer = 6; 
    int pavementLayer = 8;

    public Sprite cardSprite;

    public bool isFlipped;

    public Action<Card> Flipped;

    [SerializeField]
    Outline outline;

    public float throwGoodness;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        outline = GetComponent<Outline>();
    }

    public GameObject PickUp()
    {
        isPickedUp = true;
        //rb.isKinematic = true;
        rb.useGravity = false;
        return gameObject;
    }

    public void Release(Vector3 force, Vector3 rotationForce, float effectGoodness)
    {
        transform.parent = null;
        rb.useGravity = true;
        //rb.isKinematic = false;
        isPickedUp = false;
        rb.AddForce(force, ForceMode.Force);
        rb.AddForce(rotationForce, ForceMode.Impulse);
        throwGoodness = effectGoodness;
    }

    private void OnTriggerStay(Collider collider)
    {
        if (!isPickedUp && collider.gameObject.layer == deckLayer)
        {
            WrongThrow("Card returned to the deck");
        }

        if(!isPickedUp && collider.gameObject.layer == pavementLayer)
        {
            WrongThrow();
        }
    }

    public void Flip()
    {
        if (!isFlipped)
        {
            Vector3 rotation = transform.localRotation.eulerAngles;
            transform.localRotation = Quaternion.Euler(180, rotation.y, rotation.z);
            isFlipped = true;
            Flipped?.Invoke(this);
            DisableOutline();
            GameManager.Instance.ResetHappinessDepletionTimer();
        }
    }
    
    public void WrongThrow()
    {
        Deck.Instance.AddCard(this);
        if (!Deck.Instance.isShuffled)
        {
            UIManager.Instance.ComunicateToPlayer("First click on the deck to shuffle it", Color.red, 1.5f);
        }
        else
        {
            UIManager.Instance.ComunicateToPlayer("Wrong throw! <BR> The card is returned to the top of the deck", Color.red, 1.5f);
        }
        
    }

    public void AddTorque(float magnitude)
    {
        rb.AddTorque(transform.up * magnitude);
    }

    public void WrongThrow(string message)
    {
        Deck.Instance.AddCard(this);
        UIManager.Instance.ComunicateToPlayer(message, Color.red, 1.5f);
    }

    public void StopAllForces()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    public void EnableOutline()
    {
        outline.Blink();
    }

    public void DisableOutline()
    {
        outline.enabled = false;
    }
}
